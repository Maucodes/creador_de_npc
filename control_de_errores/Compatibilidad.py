#coding: utf-8
#author: mau


class Compatibilidad():

    def __init__(self):
        self._estado_total = True
        self._estado = {}

    def _validar_datos_ingresados(self):
        """
        Methodo utilizado para la verificacion de fallos dentro de los datos
        entregados a el objeto, para la manipulacion de contorl de errores
        :return: nada
        """
        pass

    def incorporar_soluciones(self,diccionario):
        """
        Methodo comprueba y modifica todos los datos nuevamente, pasando por las
        reglas de nogocios,
        :param diccionario: entrega un diccionario modificado con los resultados nuevos
        :return: una tupla que representa el estado total, y el diccionario total
        """
        return False,{}

    def get_estado_total(self):
        return self._estado_total

    def set_estado_total(self,valor):
        self._estado_total = valor

    def set_estados(self,diccionario):
        self._estado = diccionario

    def asignar_valor_diccionario(self,key,valor):
        if valor == False:
            self._estado_total = False
        self._estado[key]=valor

    def pedir_valor_diccionario(self,key):
        return self._estado[key]

    def get_estados(self):
        return self._estado