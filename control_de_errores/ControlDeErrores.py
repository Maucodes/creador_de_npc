#coding: utf-8
#author: mau

from kivy.uix.popup import Popup
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput


class ControlDeErrores(Popup):
    def __init__(self,lista,objeto,methodo,*args):
        super(ControlDeErrores, self).__init__(*args)
        self.lista=lista
        self.objeto = objeto
        self._configuracion_previa()
        self.control_activo = {}
        self.contenido_maya = []
        self.maya = GridLayout(cols=2)
        self.add_widget(self.maya)
        self.configuracion_de_grid()
        self.methodo = methodo


    def configuracion_de_grid(self):
        """
        Methodo utilizado para la configuracion de la interface de error
        generica como modulo para todas las demas clases, tomando encuenta
        errores de estado pre definidas en las reglas de negocio
        :return: nada
        """
        self.boton_guardar = Button(text="Guardar", on_press=self.actualizar_datos)
        self.boton_salir = Button(text="Salir", on_press=self.dismiss)
        if self.objeto.get_estado_total() == False:
            self.lista.reiniciar_lista_secuencias_xml()
            for x in self.objeto.get_estados().items():
                if x[1] == False:
                    lb = Label(text= x[0])
                    tx = TextInput()

                    self.control_activo[x[0]]= tx
                    self.maya.add_widget(lb)
                    self.maya.add_widget(tx)
                    self.contenido_maya.append(lb)
                    self.contenido_maya.append(tx)

            self.maya.add_widget(self.boton_guardar)
            self.maya.add_widget(self.boton_salir)
            self.contenido_maya.append(self.boton_salir)
            self.contenido_maya.append(self.boton_guardar)
        else:
            self.dismiss()

    def actualizar_datos(self,*args):
        """
        Methodo utilizado para objetos y listas, y gestionar la actualizacion de dichi
        objeto este objeto esta amarrado al tener un estado, estado total, estado incorporado
        :param args: argumentos necesarios apra funcionar nuestros button
        :return: nada
        """
        diccionario_contenido_de_la_clase = self.objeto.get_estados()
        for x in (self.control_activo.items()):
            self.control_activo[x[0]] = x[1].text
            diccionario_contenido_de_la_clase[x[0]] = x[1].text

        el_estado = self.objeto.incorporar_soluciones(diccionario_contenido_de_la_clase)

        if el_estado[0]:
            self.methodo(self.objeto)
            self.dismiss()
        else:
            self._limpiar_contenido()
            self.configuracion_de_grid()

    def _limpiar_contenido(self):
        """
        methodo utilizado para limpiar nuestras listas y quitar el espacio en memoria
        de dichas listas que controlan en tiempo real el widget
        :return: nada
        """
        for x in self.contenido_maya:
            self.maya.remove_widget(x)
        self.control_activo.clear()


    def _configuracion_previa(self):
        """
        Methodo utilizado para precaragr el widget
        :return: nada
        """
        self.title= "Control de errores"
        self.dismiss()
        self.auto_dismiss =  False
        self.size_hint = (.6,.5)

    def abrirPop(self):
        """
        methodo precario, para abrir el widget desde cualquier zona
        :return:
        """
        self.open()