#coding: utf-8
#author: mau

from control_de_errores.Compatibilidad import Compatibilidad


class Objetos(Compatibilidad):
    _nodo = 0
    _activacion = ""
    _mensaje = ""
    def __init__(self,nodo,activacion,mensaje):
        super(Objetos, self).__init__()
        self.set_estado_total(True)
        self.set_estados({"nodo":True,
                          "activacion":True,
                          "mensaje":True})
        self.set_nodo(nodo)
        self.set_activacion(activacion)
        self.set_mensaje(mensaje)

    def formato_objeto(self):
        return (str(self._nodo),self._activacion,self._mensaje)

    def _validar_datos_ingresados(self):
        conteo_control_de_errores = 0
        for losDatos in self.get_estados().items():
            if losDatos[1] == False:
                conteo_control_de_errores+=1
                self.set_estado_total(False)
            if conteo_control_de_errores == 0:
                self.set_estado_total(True)

    def incorporar_soluciones(self,diccionario):
        if diccionario["nodo"] != True:
            self.set_nodo(diccionario["nodo"])
        if diccionario["activacion"] != True:
            self.set_activacion(diccionario["activacion"])
        if diccionario["mensaje"] != True:
            self.set_mensaje(diccionario["mensaje"])

        self._validar_datos_ingresados()
        return self.get_estado_total(),self.get_estados()

    def set_nodo(self,valor):
        try:
            if int(valor)>= 1:
                self._nodo = valor
                self.asignar_valor_diccionario("nodo",True)
            else:
                self._nodo = 0
                self.asignar_valor_diccionario("nodo",False)
        except TypeError as error:
            self._nodo = 0
            self.asignar_valor_diccionario("nodo", False)
            print("Error: ", type(error), " Valor: ", valor, " Es Incorrecto")
        except ValueError as error:
            self._nodo = 0
            self.asignar_valor_diccionario("nodo", False)
            print("Error: ", type(error), " Valor: ", valor, " Es Incorrecto")

    def set_activacion(self,mensaje):
        try:
            if len(mensaje) >= 1:
                self._activacion = mensaje
                self.asignar_valor_diccionario("activacion",True)
            else:
                self._activacion = ""
                self.asignar_valor_diccionario("activacion",False)
        except TypeError as error:
            self._activacion = ""
            self.asignar_valor_diccionario("activacion", False)
            print("Error: ", type(error), " Valor: ", mensaje, " Es Incorrecto")
    def set_mensaje(self,mensaje):
        try:
            if len(mensaje) >= 1:
                self._mensaje = mensaje
                self.asignar_valor_diccionario("activacion",True)
            else:
                self._mensaje = ""
                self.asignar_valor_diccionario("activacion",False)
        except TypeError as error:
            self._mensaje = ""
            self.asignar_valor_diccionario("activacion", False)
            print("Error: ", type(error), " Valor: ", mensaje, " Es Incorrecto")