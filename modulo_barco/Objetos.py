#coding: utf-8
#author: mau
from control_de_errores.Compatibilidad import Compatibilidad

class Objetos(Compatibilidad):
    _nombre = ""
    _pos_x = 0
    _pos_y = 0
    _pos_z = 0
    _costo = 0
    def __init__(self,nombre,posx,posy,posz,costo):
        super(Objetos, self).__init__()
        self._solicitud_de_nivel = 0
        self.set_estado_total(True)
        self.set_estados({"nombre":True,
                          "pos_x":True,
                          "pos_y":True,
                          "pos_z":True,
                          "costo":True})
        self.set_nombre(nombre)
        self.set_pos_x(posx)
        self.set_pos_y(posy)
        self.set_pos_z(posz)
        self.set_costo(costo)

    def formato_boton_objeto(self):
        return "Nombre: {0}, x:{1}, y:{2}, z:{3}, Precio: {2};".format(self._nombre, str(self._pos_x), str(self._pos_y),str(self._pos_z),str(self._costo))

    def formato_xml(self):
        return (self._nombre,str(self._pos_x),str(self._pos_y),str(self._pos_z),str(self._costo))

    def formato_objeto(self):
        return "{0},{1},{2},{3},{4};".format(self._nombre,str(self._pos_x),str(self._pos_y),str(self._pos_z),str(self._costo))

    def _validar_datos_ingresados(self):
        conteo_de_errores = 0
        for losDatos in self.get_estados().items():
            if losDatos[1] == False:
                conteo_de_errores+=1
                self.set_estado_total(False)
            elif conteo_de_errores == 0:
                self.set_estado_total(True)

    def incorporar_soluciones(self,diccionario):
        print(diccionario)
        if diccionario["nombre"] != True:
            self.set_nombre(diccionario["nombre"])
        if diccionario["pos_x"] != True:
            self.set_pos_x(diccionario["pos_x"])
        if diccionario["pos_y"] != True:
            self.set_pos_y(diccionario["pos_y"])
        if diccionario["pos_z"] != True:
            self.set_pos_z(diccionario["pos_z"])
        if diccionario["costo"] != True:
            self.set_costo(diccionario["costo"])

        self._validar_datos_ingresados()
        print(self.get_estado_total(),self.get_estados())
        return self.get_estado_total(),self.get_estados()



    def set_costo(self,valor):
        try:
            if int(valor) >= 0:
                self._costo = valor
                self.asignar_valor_diccionario("costo",True)
            else:
                self._costo = 0
                self.asignar_valor_diccionario("costo",False)
        except TypeError as error:
            self._costo = 0
            self.asignar_valor_diccionario("costo", False)
            print("Error: ", type(error), " Valor: ", valor, " Es Incorrecto")
        except ValueError as error:
            self._costo = 0
            self.asignar_valor_diccionario("costo", False)
            print("Error: ", type(error), " Valor: ", valor, " Es Incorrecto")
    def set_pos_z(self,valor):
        try:
            if int(valor) >= 0:
                self._pos_z = valor
                self.asignar_valor_diccionario("pos_z",True)
            else:
                self._pos_z = 0
                self.asignar_valor_diccionario("pos_z",False)
        except TypeError as error:
            self._pos_z = 0
            self.asignar_valor_diccionario("pos_z", False)
            print("Error: ", type(error), " Valor: ", valor, " Es Incorrecto")
        except ValueError as error:
            self._pos_z = 0
            self.asignar_valor_diccionario("pos_z", False)
            print("Error: ", type(error), " Valor: ", valor, " Es Incorrecto")
    def set_pos_y(self,valor):
        try:
            if int(valor) >= 0:
                self._pos_y = valor
                self.asignar_valor_diccionario("pos_y",True)
            else:
                self._pos_y = 0
                self.asignar_valor_diccionario("pos_y",False)
        except TypeError as error:
            self._pos_y = 0
            self.asignar_valor_diccionario("pos_y", False)
            print("Error: ", type(error), " Valor: ", valor, " Es Incorrecto")
        except ValueError as error:
            self._pos_y = 0
            self.asignar_valor_diccionario("pos_y", False)
            print("Error: ", type(error), " Valor: ", valor, " Es Incorrecto")

    def set_pos_x(self,valor):
        try:
            if int(valor) >= 0:
                self._pos_x = valor
                self.asignar_valor_diccionario("pos_x",True)
            else:
                self._pos_x = 0
                self.asignar_valor_diccionario("pos_x",False)
        except TypeError as error:
            self._pos_x = 0
            self.asignar_valor_diccionario("pos_x", False)
            print("Error: ", type(error), " Valor: ", valor, " Es Incorrecto")
        except ValueError as error:
            self._pos_x = 0
            self.asignar_valor_diccionario("pos_x", False)
            print("Error: ", type(error), " Valor: ", valor, " Es Incorrecto")

    def set_nombre(self,nombre):
        try:
            if len(nombre) >= 3:
                self._nombre = nombre.lower()
                self.asignar_valor_diccionario("nombre",True)
            else:
                self._nombre = ""
                self.asignar_valor_diccionario("nombre", False)
        except TypeError as error:
            print("Error: ", type(error), " Valor: ", nombre, " Es Incorrecto")





