from kivy.uix.popup import Popup



class Pop_Edit_Barco(Popup):
    def __init__(self,boton,objeto,*args):
        super(Pop_Edit_Barco, self).__init__(*args)
        self.objeto = objeto
        self.boton= boton
        self.configuracion()



    def configuracion(self):
        """
        methodo que configura el pop para revisar las posibilidades de errores
        :return: nada
        """
        aux = self.objeto.formato_xml()
        self.ids.textNombre.text = aux[0]
        self.ids.textX.text = aux[1]
        self.ids.textY.text = aux[2]
        self.ids.textZ.text = aux[3]
        self.ids.textCosto.text = aux[4]

    def guardarDatos(self):
        """
        methodo que guarda todos los datos en el objeto
        :return: nada
        """
        self.objeto.set_nombre(self.ids.textNombre.text)
        self.objeto.set_pos_x(self.ids.textX.text)
        self.objeto.set_pos_y(self.ids.textY.text)
        self.objeto.set_pos_z(self.ids.textZ.text)
        self.objeto.set_costo(self.ids.textCosto.text)
        self.boton.text = self.objeto.formato_boton_objeto()

        self.dismiss()