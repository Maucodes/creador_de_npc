#coding: utf-8
#author: Mau
from kivy.uix.button import Button
from modulo_barco.Pop_Edit_Barco import Pop_Edit_Barco
class Boton_Barco(Button):
    def __init__(self, objeto,*args):
        super(Boton_Barco, self).__init__(*args)
        self.objeto = objeto
        self.text = self.objeto.formato_boton_objeto()
        self.on_press= self.actualizar


    def actualizar(self):
        """
        Methodo utilizado para editar los objetos, se entrega el boton y el objeto
        :return: nada
        """
        pop = Pop_Edit_Barco(self,self.objeto)
        pop.open()
