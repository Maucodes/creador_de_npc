#coding: utf-8
#author: mau
from kivy.uix.floatlayout import FloatLayout
from entidad.Entidad import Entidad
from control_de_errores.ControlDeErrores import ControlDeErrores
from modulo_barco.Objetos import Objetos
from modulo_barco.Boton_Barco import Boton_Barco

class NPC_Barco(FloatLayout):
    def __init__(self,lista,*args):
        super(NPC_Barco, self).__init__(*args)
        self.listas = lista

    def agregar(self, methodo = None):
        """
        methodo para asignar los objetos a las listas de secuencia de todos los objetos tipo
        barcos para asignar
        :param methodo: se utiliza para el control de errores, si este encuentra un error se agrega la corrección
        :return: nada
        """
        if methodo == None:
            objeto = Objetos(self.ids.nombreText.text,
                                                self.ids.posXText.text,
                                                self.ids.posYText.text,
                                                self.ids.posZText.text,
                                                self.ids.costoText.text)
            if objeto.get_estado_total():
                self.listas.agregar_lista_barcos(objeto)
                self.ids.cajaDepandora.add_widget(Boton_Barco(objeto))
            else:
                pop = ControlDeErrores(self.listas,objeto,self.agregar)
                pop.abrirPop()
        else:
            if methodo.get_estado_total():
                self.listas.agregar_lista_barcos(methodo)
                self.ids.cajaDepandora.add_widget(Boton_Barco(methodo))
            else:
                pop = ControlDeErrores(self.listas, methodo, self.agregar)
                pop.abrirPop()

    def guardar(self, methodo = None):
        """
        methodo para guardar informacion al archivo xml generando y comprobando errores
        :param methodo: se utilizada para control de errores, devuelve correccion
        :return: nada
        """
        if methodo == None:
            methodo = Entidad(self.ids.npcNombre.text, self.ids.npcScript.text, int(self.ids.walkinNpc.text),
                              int(self.ids.floorNpc.text), int(self.ids.typeNPc.text), int(self.ids.headNPC.text),
                              int(self.ids.bodyNPC.text), int(self.ids.legsNpc.text), int(self.ids.feetsNPC.text),
                              int(self.ids.npcAddons.text),
                              self.ids.npcMensaje.text)
            if methodo.get_estado_total():
                entidadDatos = methodo.formato_xml()
                self.listas.generar_archivo_barco_xml(methodo,entidadDatos[0],methodo.get_mensaje())#posicion 0 es el nombre
            else:
                pop = ControlDeErrores(self.listas,methodo,self.guardar)
                pop.abrirPop()
        else:
            if methodo.get_estado_total():
                entidadDatos = methodo.formato_xml()
                self.listas.generar_archivo_barco_xml(methodo,entidadDatos[0],methodo.get_mensaje())#posicion 0 es el nombre
            else:
                pop = ControlDeErrores(self.listas,methodo,self.guardar)
                pop.abrirPop()


