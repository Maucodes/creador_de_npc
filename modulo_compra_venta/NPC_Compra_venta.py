from kivy.uix.floatlayout import FloatLayout

from core.Contructora import NPC_basio
from modulo_compra_venta.Objetos import Objetos
from modulo_compra_venta.Boton_compra_venta import Boton_compra_venta
from entidad.Entidad import Entidad
from control_de_errores.ControlDeErrores import ControlDeErrores


class NPC_Compra_venta(FloatLayout):

    def __init__(self,listas,*args):
        super(NPC_Compra_venta, self).__init__(*args)
        self.listas = listas


    def guardar(self, methodo = None):
        if methodo == None:
            methodo = Entidad(self.ids.npcNombre.text,self.ids.npcScript.text,self.ids.walkinNpc.text,
                          self.ids.floorNpc.text,self.ids.typeNPc.text,self.ids.headNPC.text,
                          self.ids.bodyNPC.text,self.ids.legsNpc.text,self.ids.feetsNPC.text,self.ids.npcAddons.text,
                          self.ids.npcMensaje.text)
            if methodo.get_estado_total():
                entidadDatos = methodo.formato_xml()
                self.listas.agregar_lista_secuencias_xml(NPC_basio(entidadDatos))
                self.listas.generar_archivo_compra_venta_xml(entidadDatos[0],methodo.get_mensaje())#posicion 0 es el nombre
            else:
                pop = ControlDeErrores(self.listas,methodo,self.guardar)
                pop.abrirPop()
        else:
            if methodo.get_estado_total():
                entidadDatos = methodo.formato_xml()
                self.listas.agregar_lista_secuencias_xml(NPC_basio(entidadDatos))
                self.listas.generar_archivo_compra_venta_xml(entidadDatos[0],methodo.get_mensaje())#posicion 0 es el nombre
            else:
                pop = ControlDeErrores(self.listas,methodo,self.guardar)
                pop.abrirPop()

    def agregar_npc_vende(self,methodo = None):
        if methodo == None:
            objeto = Objetos(self.ids.objetoNombre.text, self.ids.objetoID.text,
                             self.ids.objetoPrecio.text)  # creamos posicion en memoria del objeto
            if objeto.get_estado_total() == True:
                self.listas.agregar_lista_vende(objeto)  # pasamos posicion en memoria
                self.ids.elNpcVende.add_widget(Boton_compra_venta(objeto,self.listas))
            else:
                ControlDeErrores(self.listas,objeto,self.agregar_npc_vende).open()
        else:
            if methodo.get_estado_total() == True:
                self.listas.agregar_lista_vende(methodo)  # pasamos posicion en memoria
                self.ids.elNpcVende.add_widget(Boton_compra_venta(methodo, self.listas))
            else:
                ControlDeErrores(self.listasobjeto,methodo, self.agregar_npc_vende).open()

    def agregar_npc_compra(self, methodo = None):
        if methodo == None:
            objeto = Objetos(self.ids.objetoNombre.text, self.ids.objetoID.text,
                             self.ids.objetoPrecio.text)  # creamos posicion en memoria del objeto
            if objeto.get_estado_total() == True:
                self.listas.agregar_lista_compra(objeto) # pasamos posicion en memoria
                self.ids.elNpcCompra.add_widget(Boton_compra_venta(objeto,self.listas))
            else:
                ControlDeErrores(self.listas,objeto,self.agregar_npc_compra).open()
        else:
            if methodo.get_estado_total() == True:
                self.listas.agregar_lista_compra(methodo)  # pasamos posicion en memoria
                self.ids.elNpcCompra.add_widget(Boton_compra_venta(methodo, self.listas))
            else:
                ControlDeErrores(self.listas, methodo, self.agregar_npc_compra).open()