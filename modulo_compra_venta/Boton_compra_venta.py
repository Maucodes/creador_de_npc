from kivy.uix.button import Button

from modulo_compra_venta.Pop_compra_venta import Pop_compra_venta


class Boton_compra_venta(Button):
    def __init__(self,objeto,listas,*args):
        super(Boton_compra_venta, self).__init__(*args)
        self.objeto = objeto
        self.listas = listas
        self.text = self.objeto.formato_boton_objeto()
        self.on_press = self.popBoton

    def popBoton(self):
        pop_compra_venta = Pop_compra_venta(self.__self__,self.objeto)
        pop_compra_venta.open()
