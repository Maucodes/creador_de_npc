from kivy.uix.popup import Popup

class Pop_compra_venta(Popup):
    def __init__(self,boton,objeto,*args):
        super(Pop_compra_venta,self).__init__(*args)
        self.dismiss()
        self.boton = boton
        self.objeto = objeto
        self.title = self.objeto.formato_boton_objeto()
        self.generacion()



    def generacion(self):
        self.ids.textNombre.text = self.objeto.get_nombre()
        self.ids.textID.text = str(self.objeto.get_id_objeto())
        self.ids.textPrecio.text = str(self.objeto.get_precio())

    def actualizar(self):
        self.objeto.set_nombre(self.ids.textNombre.text)
        self.objeto.set_id_objet(self.ids.textID.text)
        self.objeto.set_precio(self.ids.textPrecio.text)
        self.boton.text = self.objeto.formato_boton_objeto()
        self.dismiss()

    def eliminar(self):
        pass