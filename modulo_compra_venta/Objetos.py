#coding: utf-8
#author: mau
from control_de_errores.Compatibilidad import Compatibilidad
class Objetos(Compatibilidad):
    _nombre= ""
    _id_objeto= 0
    _precio = 0

    def __init__(self,nombre,id_objeto,precio):
        super(Objetos, self).__init__()
        self.set_estado_total(True)
        self.set_estados({"nombre":True,
                          "id_objeto":True,
                          "precio":True})
        self.set_nombre(nombre)
        self.set_id_objet(id_objeto)
        self.set_precio(precio)

    def formato_objeto(self):
        return (self._nombre,str(self._id_objeto),str(self._precio))

    def _validar_datos_ingresados(self):
        conteo_de_errores =0
        for losDatos in self.get_estados().items():
            if losDatos[1] == False:
                conteo_de_errores+=1
                self.set_estado_total(False)
            if conteo_de_errores == 0:
                self.set_estado_total(True)

    def incorporar_soluciones(self,diccionario):
        if diccionario["nombre"] != True:
            self.set_nombre(diccionario["nombre"])
        if diccionario["id_objeto"] != True:
            self.set_id_objet(diccionario["id_objeto"])
        if diccionario["precio"] != True:
            self.set_precio(diccionario["precio"])

            self._validar_datos_ingresados()
        return self.get_estado_total(),self.get_estados()

    def set_nombre(self,nombre):
        try:
            if len(nombre) >= 1:
                self._nombre = nombre.lower()
                self.asignar_valor_diccionario("nombre", True)
            else:
                self._nombre = 0
                self.asignar_valor_diccionario("nombre", False)
        except TypeError as error:
            self._nombre = 0
            self.asignar_valor_diccionario("nombre", False)
        except ValueError as error:
            self._nombre = 0
            self.asignar_valor_diccionario("nombre", False)
    def set_id_objet(self,id_objeto):
        try:
            if int(id_objeto) >= 1:
                self._id_objeto =id_objeto
                self.asignar_valor_diccionario("id_objeto",True)
            else:
                self._id_objeto = 0
                self.asignar_valor_diccionario("id_objeto",False)
        except TypeError as error:
            self._id_objeto = 0
            self.asignar_valor_diccionario("id_objeto", False)
        except ValueError as error:
            self._id_objeto = 0
            self.asignar_valor_diccionario("id_objeto", False)
    def set_precio(self,precio):
        try:
            if int(precio) >= 1:
                self._precio =precio
                self.asignar_valor_diccionario("precio",True)
            else:
                self._precio = 0
                self.asignar_valor_diccionario("precio",False)
        except TypeError as error:
            self._precio = 0
            self.asignar_valor_diccionario("precio", False)
        except ValueError as error:
            self._precio = 0
            self.asignar_valor_diccionario("precio", False)


    def formato_objeto(self):
        return "{0},{1},{2};".format(self._nombre,self._id_objeto,self._precio)

    def formato_boton_objeto(self):
        return "Nombre: {0}, ID: {1}, Precio: {2};".format(self._nombre, self._id_objeto, self._precio)

    def get_nombre(self):
        return self._nombre
    def get_id_objeto(self):
        return self._id_objeto
    def get_precio(self):
        return self._precio