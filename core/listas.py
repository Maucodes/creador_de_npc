#coding: utf-8
#author: mau
from core.Archivos import *
from core.Contructora import *

class Listas():

    def __init__(self):
        self._lista_secuencias_xml = []
        self._lista_secuencias_lua = []
        self._lista_barcos = []
        self._lista_compra = []
        self._lista_vende = []
        self.npc_compra_total=""
        self.npc_vende_total=""
        self.npc_barco = ""

    def agregar_lista_secuencias_xml(self,valor):
        self._lista_secuencias_xml.append(valor)

    def agregar_lista_secuencias_lua(self,valor):
        self._lista_secuencias_lua.append(valor)

    def agregar_lista_barcos(self,valor):
        self._lista_barcos.append(valor)

    def agregar_lista_compra(self,valor):
        self._lista_compra.append(valor)

    def agregar_lista_vende(self,valor):
        self._lista_vende.append(valor)

    def reiniciar_todas_las_listas(self):
        self._lista_vende.clear()
        self._lista_compra.clear()
        self._lista_barcos.clear()
        self._lista_secuencias_lua.clear()
        self._lista_secuencias_xml.clear()
    def reiniciar_lista_vende(self):
        self._lista_vende.clear()
    def reiniciar_lista_compra(self):
        self._lista_compra.clear()
    def reiniciar_lista_barco(self):
        self._lista_barcos.clear()
    def reiniciar_lista_secuencias_xml(self):
        self._lista_secuencias_xml.clear()
    def reiniciar_listas_secuencias_lua(self):
        self._lista_secuencias_lua.clear()

    def eliminar_elemento_lista_secuencia_xml(self,elemento):
        self._lista_secuencias_xml.remove(elemento)

    def eliminar_elemento_lista_secuencia_lua(self,elemento):
        self._lista_secuencias_lua.remove(elemento)

    def eliminar_elemento_lista_barco(self,elemento):
        self._lista_compra.remove(elemento)

    def eliminar_elemento_lista_compra(self,elemento):
        self._lista_compra.remove(elemento)

    def eliminar_elemento_lista_vende(self,elemento):
        self._lista_vende.remove(elemento)

    def generar_archivo_barco_xml(self,entidad,nombre,mensaje):
        self.agregar_lista_secuencias_xml(NPC_basio(entidad.formato_xml()))
        for archivos in self._lista_barcos:
            self.npc_barco+= archivos.formato_objeto()
        self.agregar_lista_secuencias_xml(funcion_barco((mensaje,self.npc_barco)))
        self.agregar_lista_secuencias_xml(FinNPC())
        funcion_guardar_archivo_xml(nombre,self._lista_secuencias_xml)

    def generar_archivo_compra_venta_xml(self,nombre,mensaje):
        for archivos in self._lista_compra:
            self.npc_compra_total += archivos.formato_objeto()
        for archivos in self._lista_vende:
            self.npc_vende_total += archivos.formato_objeto()
        self.agregar_lista_secuencias_xml(funcion_compra_venta((mensaje,self.npc_compra_total,self.npc_vende_total)))
        self.agregar_lista_secuencias_xml(FinNPC())
        funcion_guardar_archivo_xml(nombre,self._lista_secuencias_xml)

