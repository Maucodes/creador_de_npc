#coding:utf-8
#author: mau
from kivy.lang.builder import Builder

def agregar_compra_venta():
    return Builder.load_file("modulo_compra_venta/modulo_compra_venta.kv")

def agregar_modulo_barco():
    return Builder.load_file("modulo_barco/modulo_barco.kv")

def agregar_modulo_keyword():
    return Builder.load_file("modulo_keyword/modulo_keyword.kv")
