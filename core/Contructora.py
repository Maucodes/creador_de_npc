# coding: utf-8
# author: mau


def NPC_basio(*args):
    return """
    <?xml version="1.0" encoding="UTF-8"?>
        <npc name="{0}" script="{1}" walkinterval="{2}" floorchange="{3}">
	    <health now="100" max="100" />
	    <look type="{4}" head="{5}" body="{6}" legs="{7}" feet="{8}" addons="{9}" />
    """.format(*args[0])


def funcion_barco(*args):
    return """
	        <parameters>
                <parameter key="module_travel" value="1"/>
                <parameter key="message_greet" value="{0}."/>
                <parameter key="travel_destinations" value="{1}"/>
	        </parameters>
	        
    """.format(*args[0])


def fucion_keyword_nodo_principal(nodoprincipal, activacion, mensaje):
    return """
    local node%s = keywordHandler:addKeyword({'%s'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = '%s'})
    """%(nodoprincipal, activacion, mensaje)


def funcion_keyword_hijos(nodo, activacion, mensaje):
    return """
    node%s:addChildKeyword({'%s'}, StdModule.say, {npcHandler = npcHandler, text = '%s'})
    """ % (nodo, activacion, mensaje)


def funcion_compra_venta(*args):
    return """
    	<parameters>
		<parameter key="module_shop" value="1" />
		<parameter key="message_greet" value="{0}"/>
		<parameter key="shop_sellable" value="{1}" />
		<parameter key="shop_buyable" value="{2}" />
	</parameters>
    """.format(*args[0])


def lua_cabezera():
    return """

    local keywordHandler = KeywordHandler:new()
    local npcHandler = NpcHandler:new(keywordHandler)
    NpcSystem.parseParameters(npcHandler)

    function onCreatureAppear(cid)              npcHandler:onCreatureAppear(cid)            end
    function onCreatureDisappear(cid)           npcHandler:onCreatureDisappear(cid)         end
    function onCreatureSay(cid, type, msg)      npcHandler:onCreatureSay(cid, type, msg)    end
    function onThink()                          npcHandler:onThink()                        end

    """


def lua_accion_jugador(*args):
    return """
    local function creatureSayCallback(cid, type, msg)
        if(not npcHandler:isFocused(cid)) then
        return false
	end

    local player = Player(cid)
	if msg:lower() == "{0}" then
        npcHandler.topic[cid] = {1}
		return npcHandler:say("{2}", cid)

    """.format(*args[0])


def FinNPC():
    return """
            </npc>"""
