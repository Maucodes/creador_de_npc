import kivy,sys
kivy.require("1.11.1")

from kivy.app import App
from kivy.lang.builder import Builder
from loby.Loby import Loby
from core.listas import Listas


#Main
ventana = None
class MiApp(App):
    title = "Generador de NPCs"
    listados= Listas()

    def build(self):

        self.root = Builder.load_file("loby/loby.kv")
        return Loby(ventana,self.listados)
if __name__ == "__main__":
    ventana = MiApp()
    ventana.run()
