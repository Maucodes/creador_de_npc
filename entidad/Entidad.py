#coding: utf-8
#author: mau
from control_de_errores.Compatibilidad import Compatibilidad

class Entidad(Compatibilidad):
    _nombre = ""
    _scripts= ""
    _walkinterval = 0
    _floorchange = 0
    _type = 0
    _head = 0
    _body = 0
    _legs = 0
    _feet = 0
    _addons = 0
    _mensaje = ""

    def __init__(self,nombre,scripts,walking,floor,type,head,body,legs,feets,addons,mensaje):
        super(Entidad, self).__init__()

        self._estado_total = True
        self.set_estados({"nombre": True,
                  "scripts": True,
                  "walking": True,
                  "floor": True,
                  "type": True,
                  "head": True,
                  "body": True,
                  "legs": True,
                  "feet": True,
                  "addons": True,
                  "mensaje": True})
        self.set_nombre(nombre)
        self.set_scripts(scripts)
        self.set_walking(walking)
        self.set_floor(floor)
        self.set_type(type)
        self.set_head(head)
        self.set_body(body)
        self.set_legs(legs)
        self.set_feet(feets)
        self.set_addons(addons)
        self.set_mensaje(mensaje)

    def _validar_datos_ingresados(self):
        """
        Methodo utilizado para la verificacion de fallos dentro de los datos
        entregados a el objeto, para la manipulacion de contorl de errores
        :return: nada
        """
        estado_provisorio = False
        conteo_de_errores = 0
        for losDatos in self.get_estados().items():
            if losDatos[1] == False:
                conteo_de_errores +=1
                self.set_estado_total(False)
            elif conteo_de_errores == 0:
                self.set_estado_total(True)



    def incorporar_soluciones(self,diccionario):
        """
        Methodo comprueba y modifica todos los datos nuevamente, pasando por las
        reglas de nogocios,
        :param diccionario: entrega un diccionario modificado con los resultados nuevos
        :return: una tupla que representa el estado total, y el diccionario total
        """
        if diccionario["nombre"] != True:
            self.set_nombre(diccionario["nombre"])
        if diccionario["scripts"] != True:
            self.set_scripts(diccionario["scripts"])
        if diccionario["walking"] != True:
            self.set_walking(diccionario["walking"])
        if diccionario["floor"] != True:
            self.set_floor(diccionario["floor"])
        if diccionario["type"] != True:
            self.set_type(diccionario["type"])
        if diccionario["head"] != True:
            self.set_head(diccionario["head"])
        if diccionario["body"] != True:
            self.set_body(diccionario["body"])
        if diccionario["legs"] != True:
            self.set_legs(diccionario["legs"])
        if diccionario["feet"] != True:
            self.set_feet(diccionario["feet"])
        if diccionario["addons"] != True:
            self.set_addons(diccionario["addons"])
        if diccionario["mensaje"] != True:
            self.set_mensaje(diccionario["mensaje"])

        self._validar_datos_ingresados()
        return self.get_estado_total(),self.get_estados()

    def set_nombre(self,nombre):
        try:
            if len(nombre) >= 3:
                self._nombre = nombre
                self.asignar_valor_diccionario("nombre",True)
            else:
                self._nombre= ""
                self.asignar_valor_diccionario("nombre", False)
        except TypeError as error:
            self._nombre = ""
            self.asignar_valor_diccionario("nombre", False)
            print("Error: ",type(error), " Valor: ",nombre ," Es Incorrecto")
    def set_scripts(self,scripts):
        try:
            if len(scripts) >= 3:
                self._scripts=scripts
                self.asignar_valor_diccionario("scripts",True)
            else:
                self._scripts="default.lua"
                self.asignar_valor_diccionario("scripts",False)
        except TypeError as error:
            self._scripts = "default.lua"
            self.asignar_valor_diccionario("scripts", False)
            print("Error: ", type(error), " Valor: ", scripts, " Es Incorrecto")

    def set_walking(self,valor):
        try:
            if int(valor) >= 0:
                self._walkinterval=valor
                self.asignar_valor_diccionario("walking",True)
            else:
                self._walkinterval= 0
                self.asignar_valor_diccionario("walking",False)
        except TypeError as error:
            self._walkinterval = 0
            self._walkinterval = 0
            self.asignar_valor_diccionario("walking", False)
            print("Error: ", type(error), " Valor: ", str(valor), " Es Incorrecto")
        except ValueError as error:
            self._walkinterval = 0
            self._walkinterval = 0
            self.asignar_valor_diccionario("walking", False)
            print("Error: ", type(error), " Valor: ", str(valor), " Es Incorrecto")
    def set_floor(self,valor):
        try:
            if int(valor) >= 0:
                self._floorchange=valor
                self.asignar_valor_diccionario("floor",True)
            else:
                self._floorchange= 0
                self.asignar_valor_diccionario("floor", False)
        except TypeError as error:
            self._floorchange = 0
            self.asignar_valor_diccionario("floor", False)
            print("Error: ", type(error), " Valor: ", str(valor), " Es Incorrecto")
        except ValueError as error:
            self._floorchange = 0
            self.asignar_valor_diccionario("floor", False)
            print("Error: ", type(error), " Valor: ", str(valor), " Es Incorrecto")

    def set_type(self,valor):
        try:
            if int(valor) >= 0:
                self._type=valor
                self.asignar_valor_diccionario("type",True)
            else:
                self._type= 0
                self.asignar_valor_diccionario("type",False)
        except TypeError as error:
            self._type = 0
            self.asignar_valor_diccionario("type",False)
            print("Error: ", type(error), " Valor: ", str(valor), " Es Incorrecto")
        except ValueError as error:
            self._type = 0
            self.asignar_valor_diccionario("type",False)
            print("Error: ", type(error), " Valor: ", str(valor), " Es Incorrecto")

    def set_head(self,valor):
        try:
            if int(valor) >= 0:
                self._head=valor
                self.asignar_valor_diccionario("head",True)
            else:
                self._head= 0
                self.asignar_valor_diccionario("head",False)
        except TypeError as error:
            self._head = 0
            self.asignar_valor_diccionario("head",False)
            print("Error: ", type(error), " Valor: ", str(valor), " Es Incorrecto")
        except ValueError as error:
            self._head = 0
            self.asignar_valor_diccionario("head",False)
            print("Error: ", type(error), " Valor: ", str(valor), " Es Incorrecto")

    def set_body(self,valor):
        try:
            if int(valor) >= 0:
                self._body=valor
                self.asignar_valor_diccionario("body",True)
            else:
                self._body= 0
                self.asignar_valor_diccionario("body",False)
        except TypeError as error:
            self._body = 0
            self.asignar_valor_diccionario("body",False)
            print("Error: ", type(error), " Valor: ", str(valor), " Es Incorrecto")
        except ValueError as error:
            self._body = 0
            self.asignar_valor_diccionario("body",False)
            print("Error: ", type(error), " Valor: ", str(valor), " Es Incorrecto")

    def set_legs(self,valor):
        try:
            if int(valor) >= 0:
                self._legs=valor
                self.asignar_valor_diccionario("legs",True)
            else:
                self._legs= 0
                self.asignar_valor_diccionario("legs",False)
        except TypeError as error:
            self._legs = 0
            self.asignar_valor_diccionario("legs",False)
            print("Error: ", type(error), " Valor: ", str(valor), " Es Incorrecto")
        except ValueError as error:
            self._legs = 0
            self.asignar_valor_diccionario("legs",False)
            print("Error: ", type(error), " Valor: ", str(valor), " Es Incorrecto")
    def set_feet(self,valor):
        try:
            if int(valor) >= 0:
                self._feet=valor
                self.asignar_valor_diccionario("feet",True)
            else:
                self._feet= 0
                self.asignar_valor_diccionario("feet",False)
        except TypeError as error:
            self._feet = 0
            self.asignar_valor_diccionario("feet",False)
            print("Error: ", type(error), " Valor: ", str(valor), " Es Incorrecto")
        except ValueError as error:
            self._feet = 0
            self.asignar_valor_diccionario("feet",False)
            print("Error: ", type(error), " Valor: ", str(valor), " Es Incorrecto")
    def set_addons(self,valor):
        try:
            if int(valor) >= 0:
                self._addons = valor
                self.asignar_valor_diccionario("addons",True)
            else:
                self._addons = 0
                self.asignar_valor_diccionario("addons",False)
        except TypeError as error:
            self._addons = 0
            self.asignar_valor_diccionario("addons",False)
            print("Error: ", type(error), " Valor: ", str(valor), " Es Incorrecto")
        except ValueError as error:
            self._addons= 0
            self.asignar_valor_diccionario("addons",False)
            print("Error: ", type(error), " Valor: ", str(valor), " Es Incorrecto")

    def set_mensaje(self,mensaje):
        try:
            if len(mensaje) >= 3:
                self._mensaje = mensaje
                self.asignar_valor_diccionario("mensaje",True)
            else:
                self._mensaje= ""
                self.asignar_valor_diccionario("mensaje",False)
        except TypeError as error:
            self._mensaje = ""
            self.asignar_valor_diccionario("mensaje",False)
            print("Error: ", type(error), " Valor: ", mensaje, " Es Incorrecto")


    def formato_solo_nombre(self):
        return "Nombre: {0}".format(self._nombre)


    def formato_xml(self):
        return (self._nombre,self._scripts,str(self._walkinterval),
                str(self._floorchange),str(self._type),str(self._head),str(self._body),
                str(self._legs),str(self._feet),str(self._addons))

    def get_mensaje(self):
        return self._mensaje