import sys

from kivy.uix.floatlayout import FloatLayout

from loby.PopAcerca import PopAcerca
from loby.PopImportante import PopImportante
from loby.PopNuevo import PopNuevo


class Loby(FloatLayout):
    def __init__(self,ventana, listas,*args):
        super(Loby,self).__init__(*args)
        self.ventana = ventana
        self.listas=listas

    def salir(self):
        sys.exit(0)

    def nuevo(self):
        nuevo = PopNuevo(self.ventana,self.listas)
        nuevo.open()

    def acerca(self):
        acerca = PopAcerca()
        acerca.open()

    def importante(self):
        importante = PopImportante()
        importante.open()

