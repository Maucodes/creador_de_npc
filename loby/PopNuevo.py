from kivy.uix.popup import Popup
from modulo_compra_venta.NPC_Compra_venta import NPC_Compra_venta
from core.interfaces import *
from modulo_barco.NPC_Barco import NPC_Barco
from modulo_keyword.Modulo_Keyword import Modulo_Keyword

class PopNuevo(Popup):
    def __init__(self,ventana,listas, *args):
        super(PopNuevo,self).__init__(*args)
        self.ventana = ventana
        self.listas = listas


    def crear_barco(self):
        self.ventana.root_window.remove_widget(self)
        self.ventana.root_window.remove_widget(self.ventana.root)
        self.ventana.root= agregar_modulo_barco()
        self.ventana.root_window.add_widget(NPC_Barco(self.listas))

    def crear_compra_venta(self):
        self.ventana.root_window.remove_widget(self)
        self.ventana.root_window.remove_widget(self.ventana.root)
        self.ventana.root = agregar_compra_venta()
        self.ventana.root_window.add_widget(NPC_Compra_venta(self.listas))


    def crear_keywords(self):
        self.ventana.root_window.remove_widget(self)
        self.ventana.root_window.remove_widget(self.ventana.root)
        self.ventana.root = agregar_modulo_keyword()
        self.ventana.root_window.add_widget(Modulo_Keyword(self.listas))
    def crear_lua(self):
        self.ventana.root_window.remove_widget(self)
        self.ventana.root_window.remove_widget(self.ventana.root)
        self.ventana.root_window.add_widget(luaSimplifica())